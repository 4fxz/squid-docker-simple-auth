FROM ubuntu:20.04
MAINTAINER Rob Haswell <me@robhaswell.co.uk>

RUN mkdir /proxy
WORKDIR /proxy
RUN apt-get -qqy update
RUN apt-get -qqy upgrade
ARG TZ
RUN ln -snf /usr/share/zoneinfo/${TZ} /etc/localtime && echo ${TZ} > /etc/timezone
RUN apt-get -qqy install apache2-utils squid3

COPY ./squid.conf /etc/squid
COPY ./run.sh .
RUN chmod +x /proxy/run.sh

RUN touch /etc/squid/squid_passwd
CMD ["/proxy/run.sh"]
