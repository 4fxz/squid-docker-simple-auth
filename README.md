# squid-docker-simple-auth


docker-compose:

```
version: "2"
services:
  squid:
    build: .
    ports:
      - 3128:3128
    environment:
      - TZ=Europe/Moscow
      - SQUID_USERNAME=foo
      - SQUID_PASSWORD=bar
```

Details
=======

Environment variables
---------------------

* TZ
* SQUID_USERNAME
* SQUID_PASSWORD

Ports
-----

* 3128

Volumes
-------

* `/var/log/squid3`
